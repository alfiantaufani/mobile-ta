-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 30 Mar 2021 pada 08.24
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id14052259_datagis`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_monitoring`
--

CREATE TABLE `tbl_monitoring` (
  `id` int(11) NOT NULL,
  `nilai_sensor` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `tgl` date NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_monitoring`
--

INSERT INTO `tbl_monitoring` (`id`, `nilai_sensor`, `tgl`, `status`) VALUES
(1, '32', '2021-02-22', 'Sukses'),
(3, '20', '2021-03-03', 'Sukses'),
(4, '2', '2021-03-03', 'Sukses'),
(411, '10', '2021-03-06', 'Sukses'),
(412, '20', '2021-03-06', 'Sukses'),
(418, '0', '2021-03-06', 'Sukses'),
(601, '10', '2021-03-08', 'Sukses'),
(947, '0', '2021-03-28', 'Sukses'),
(948, '0', '2021-03-28', 'Sukses'),
(949, '0', '2021-03-28', 'Sukses'),
(950, '0', '2021-03-28', 'Sukses'),
(951, '0', '2021-03-28', 'Sukses'),
(952, '0', '2021-03-28', 'Sukses'),
(953, '0', '2021-03-28', 'Sukses'),
(954, '0', '2021-03-28', 'Sukses'),
(955, '0', '2021-03-28', 'Sukses'),
(956, '0', '2021-03-28', 'Sukses'),
(957, '0', '2021-03-28', 'Sukses'),
(958, '0', '2021-03-28', 'Sukses'),
(959, '0', '2021-03-28', 'Sukses'),
(960, '0', '2021-03-28', 'Sukses'),
(961, '0', '2021-03-28', 'Sukses'),
(962, '0', '2021-03-28', 'Sukses'),
(963, '0', '2021-03-28', 'Sukses'),
(964, '0', '2021-03-28', 'Sukses'),
(965, '0', '2021-03-28', 'Sukses'),
(966, '0', '2021-03-28', 'Sukses'),
(967, '0', '2021-03-28', 'Sukses'),
(968, '0', '2021-03-28', 'Sukses'),
(969, '0', '2021-03-28', 'Sukses'),
(970, '0', '2021-03-28', 'Sukses'),
(971, '0', '2021-03-28', 'Sukses'),
(972, '0', '2021-03-28', 'Sukses'),
(973, '0', '2021-03-28', 'Sukses'),
(974, '0', '2021-03-28', 'Sukses'),
(975, '0', '2021-03-28', 'Sukses');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_rpm`
--

CREATE TABLE `tbl_rpm` (
  `id_rpm` int(11) NOT NULL,
  `liter_menit` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `mili_liter` varchar(25) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_rpm`
--

INSERT INTO `tbl_rpm` (`id_rpm`, `liter_menit`, `mili_liter`) VALUES
(1, '1.33', '912');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_monitoring`
--
ALTER TABLE `tbl_monitoring`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_rpm`
--
ALTER TABLE `tbl_rpm`
  ADD PRIMARY KEY (`id_rpm`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_monitoring`
--
ALTER TABLE `tbl_monitoring`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=976;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
