<?php
header('Access-Control-Allow-Origin: *');
    include "koneksi.php";
    $fungsi = $_REQUEST["fungsi"];

	    if($fungsi == "hari_ini"){
        $data = array();
        $tgl = date('Y-m-d');
        $sql = "SELECT (SELECT SUM(nilai_sensor) FROM tbl_monitoring_ruang_a WHERE tgl='$tgl') + (SELECT SUM(nilai_sensor) FROM tbl_monitoring_ruang_b WHERE tgl='$tgl') AS result";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    
    if($fungsi == "minggu_ini"){
        $data = array();
        $sql = "SELECT (SELECT SUM(nilai_sensor) FROM tbl_monitoring_ruang_a WHERE YEARWEEK(tgl)=YEARWEEK(NOW()) GROUP BY YEARWEEK(tgl)) + (SELECT SUM(nilai_sensor) FROM tbl_monitoring_ruang_b WHERE YEARWEEK(tgl)=YEARWEEK(NOW()) GROUP BY YEARWEEK(tgl)) AS result";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    
    if($fungsi == "bulan_ini"){
        $data = array();
        $sql = "SELECT (SELECT SUM(nilai_sensor) FROM tbl_monitoring_ruang_a WHERE CONCAT(YEAR(tgl),'/',MONTH(tgl))=CONCAT(YEAR(NOW()),'/',MONTH(NOW())) GROUP BY YEAR(tgl),MONTH(tgl)) + (SELECT SUM(nilai_sensor) FROM tbl_monitoring_ruang_b WHERE CONCAT(YEAR(tgl),'/',MONTH(tgl))=CONCAT(YEAR(NOW()),'/',MONTH(NOW())) GROUP BY YEAR(tgl),MONTH(tgl)) AS result";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }

	
	// JSON Untuk Ruang A
    if($fungsi == "data_monitoring_ruang_a"){
        $data = array();
        $sql = "SELECT id, status, SUM(nilai_sensor) AS nilai_sensor, DATE(tgl) tgl FROM tbl_monitoring_ruang_a GROUP BY tgl ORDER BY id DESC";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    
    if($fungsi == "rpm_ruang_a"){
        $data = array();
        $sql = "SELECT * FROM tbl_rpm WHERE id_rpm='1' ORDER BY id_rpm DESC";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    // END JSON untuk Ruang A
	
	// ---------------------------------------------------------------------------------------------------------------------------------//
	
	//JSON Untuk Ruang B

	if($fungsi == "data_monitoring_ruang_b"){
        $data = array();
        $sql = "SELECT id, status, SUM(nilai_sensor) AS nilai_sensor, DATE(tgl) tgl FROM tbl_monitoring_ruang_b GROUP BY tgl ORDER BY id DESC";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
    
    if($fungsi == "rpm_ruang_b"){
        $data = array();
        $sql = "SELECT * FROM tbl_rpm WHERE id_rpm='2' ORDER BY id_rpm DESC";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }
	
	//END JSON Untuk Ruang B
?>
