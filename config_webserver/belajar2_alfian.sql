-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 02 Jul 2021 pada 23.54
-- Versi server: 10.3.29-MariaDB-cll-lve
-- Versi PHP: 7.4.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajar2_alfian`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_monitoring_ruang_a`
--

CREATE TABLE `tbl_monitoring_ruang_a` (
  `id` int(11) NOT NULL,
  `nilai_sensor` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `tgl` date NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_monitoring_ruang_a`
--

INSERT INTO `tbl_monitoring_ruang_a` (`id`, `nilai_sensor`, `tgl`, `status`) VALUES
(2, '40', '2021-07-08', 'Sukses'),
(3, '20', '2021-07-01', 'Sukses'),
(4, '20', '2021-07-01', 'Sukses'),
(5, '20', '2021-07-01', 'Sukses');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_monitoring_ruang_b`
--

CREATE TABLE `tbl_monitoring_ruang_b` (
  `id` int(11) NOT NULL,
  `nilai_sensor` varchar(25) NOT NULL,
  `tgl` date NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_monitoring_ruang_b`
--

INSERT INTO `tbl_monitoring_ruang_b` (`id`, `nilai_sensor`, `tgl`, `status`) VALUES
(1, '10', '2021-06-30', 'sukses'),
(2, '20', '2021-07-01', 'Sukses'),
(3, '30', '2021-07-02', 'Sukses');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_rpm`
--

CREATE TABLE `tbl_rpm` (
  `id_rpm` int(11) NOT NULL,
  `liter_menit` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `mili_liter` varchar(25) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_rpm`
--

INSERT INTO `tbl_rpm` (`id_rpm`, `liter_menit`, `mili_liter`) VALUES
(1, '20', '20'),
(2, '10', '20');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_monitoring_ruang_a`
--
ALTER TABLE `tbl_monitoring_ruang_a`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_monitoring_ruang_b`
--
ALTER TABLE `tbl_monitoring_ruang_b`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_rpm`
--
ALTER TABLE `tbl_rpm`
  ADD PRIMARY KEY (`id_rpm`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_monitoring_ruang_a`
--
ALTER TABLE `tbl_monitoring_ruang_a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_monitoring_ruang_b`
--
ALTER TABLE `tbl_monitoring_ruang_b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
