hari_ini();
minggu_ini();
bulan_ini();

function hari_ini() {
    $.getJSON('https://alfianapps.000webhostapp.com/api/json.php', {fungsi: "hari_ini"}, function(result){
        var hari_ini = "";
        $.each(result, function(i, k){
            var nilai_sensor = k.result;// total
            var liter = " Liter";
            if (nilai_sensor > 0) {
                hari_ini += nilai_sensor + liter;
            }else{
                hari_ini += '0' + liter;
            }
        });
        $("#hari_ini").html(hari_ini);
    });
}setInterval(hari_ini, 5000);

function minggu_ini() {
    $.getJSON('https://alfianapps.000webhostapp.com/api/json.php', {fungsi: "minggu_ini"}, function(result){
        var minggu_ini = "";
        $.each(result, function(i, k){
            var nilai_sensor = k.result;// total
            var liter = " Liter";
            if (nilai_sensor > 0) {
                minggu_ini += nilai_sensor + liter;
            }else{
                minggu_ini += '0' + liter;
            }
        });
        $("#minggu_ini").html(minggu_ini);
    });
}setInterval(minggu_ini, 5000);

function bulan_ini() {
    $.getJSON('https://alfianapps.000webhostapp.com/api/json.php', {fungsi: "bulan_ini"}, function(result){
        var bulan_ini = "";
        $.each(result, function(i, k){
            var nilai_sensor = k.result;// total
            var liter = " Liter";
            if (nilai_sensor > 0) {
                bulan_ini += nilai_sensor + liter;
            }else{
                bulan_ini += '0' + liter;
            }
        });
        $("#bulan_ini").html(bulan_ini);
    });
}setInterval(bulan_ini, 5000);

// --------------------- RUANG A ------------------------------------------
rpm_ruang_a();
history_ruang_a();

function rpm_ruang_a(){
    $.getJSON('https://alfianapps.000webhostapp.com/api/json.php', {fungsi: "rpm_ruang_a"}, function(result){
        var g1;
        var dataa = [];
        var ml = "";
        $.each(result, function(i, k){
            var id = k.id_rpm;
            var liter_menit = k.liter_menit; //rpm
            dataa.push(liter_menit);
            $(".GaugeMeter").gaugeMeter();
        });$("#GaugeMeterRruang_a").gaugeMeter({percent:dataa});
    });
}setInterval(rpm_ruang_a, 500);        

function ml_ruang_a() {
    $.getJSON('https://alfianapps.000webhostapp.com/api/json.php', {fungsi: "rpm_ruang_a"}, function(result){
        var dataa = [];
        var ml = "";
        $.each(result, function(i, k){
            var id = k.id_rpm;
            var mili_liter = k.mili_liter;// total

            ml += mili_liter + ' mL';                    
        });
        $("#ml_ruang_a").html(ml);
    });
}setInterval(ml_ruang_a, 500);

function history_ruang_a() {
    var hasil = "";
    $.getJSON('https://alfianapps.000webhostapp.com/api/json.php', {fungsi: "data_monitoring_ruang_a"}, function(result){
		$.each(result, function(i, kolom){
            var id = kolom.id;
            var nilai_sensor = kolom.nilai_sensor;
            var tgl = kolom.tgl;
            var status = kolom.status;

            var today = new Date();
            var dd = today.getDate();

            var mm = today.getMonth()+1; 
            var yyyy = today.getFullYear();
            if(dd<10) 
            {
                dd='0'+dd;
            } 

            if(mm<10) 
            {
                mm='0'+mm;
            } 
            today = yyyy+'-'+mm+'-'+dd;

            if(tgl === today) 
            {
                hasil += '<a href="#" class="list-group-item" >\
                        <div class="row" style="padding: 20px;">\
                            <div class="col-20">\
                                <figure class="avatar avatar-40 bgra">\
                                    <b><span class="material-icons" style="color:#fff;">done</span></b>\
                                </figure>\
                            </div>\
                            <div class="col-80 pl-0 align-self-center">\
                                <h6 class="mb-1 font-weight-normal">\
                                    <b>'+ nilai_sensor +' Liter Air</b>\
                                </h6>\
                                <p class="small text-mute">\
                                    Pada Hari ini ('+tgl+')<br> \
                                    Status :<b> '+ status +' </b> \
                                    <span class="float-right ml-2 text-mute small popup-close" data-kode="'+tgl+'" onclick="hapus_a(this)"><i class="icon material-icons md-only">delete</i></span>\
                                </p>\
                            </div>\
                        </div>\
                    </a>';
            }else{
                hasil += '<a href="#" class="list-group-item" >\
                        <div class="row" style="padding: 20px;">\
                            <div class="col-20">\
                                <figure class="avatar avatar-40 bgra">\
                                    <b><span class="material-icons" style="color:#fff;">done</span></b>\
                                </figure>\
                            </div>\
                            <div class="col-80 pl-0 align-self-center">\
                                <h6 class="mb-1 font-weight-normal">\
                                    <b>'+ nilai_sensor +' Liter Air</b>\
                                </h6>\
                                <p class="small text-mute">\
                                    Pada tanggal ' + tgl + ' <br> \
                                    Status :<b> '+ status +' </b> \
                                    <span class="float-right ml-2 text-mute small popup-close" data-kode="'+tgl+'" onclick="hapus_a(this)"><i class="icon material-icons md-only">delete</i></span>\
                                </p>\
                            </div>\
                        </div>\
                    </a>';
            }
            
        });
		$("#data").html(hasil);
	});        
}setInterval(history_ruang_a, 2500);
         
// menampilkan grafik ruang A
$.getJSON('https://alfianapps.000webhostapp.com/api/json.php', {fungsi: "data_monitoring_ruang_a"}, function(result){
    var nilai_sensorr = [];
    var tgll = [];
    $.each(result, function(i, kolom){
        var id = kolom.id;
        var nilai_sensor = kolom.nilai_sensor;
        var tgl = kolom.tgl;

        nilai_sensorr.push(nilai_sensor);
        tgll.push(tgl);
    });
    
    grafik_a(nilai_sensorr, tgll);
});

function grafik_a(nilai_sensorr, tgll) {

   var options2 = {
        chart: {
            height: 350,
            type: 'area',
        },
        dataLabels: {
            enabled: false,
        },
        stroke: {
            curve: 'smooth'
        },
        series: [
                  {
                      name: 'Penggunaan Air perLiter',
                      data: nilai_sensorr
                  } 
                ],

        xaxis: {
            type: 'datetime',
            categories: tgll,
            labels: {
              style: {
                colors: 'rgba(94, 96, 110, .5)'
              }
            }           
        },
        tooltip: {
            theme:'dark',
            x: {
                format: 'dd-MM-yyyy'
            },
        },
        grid: {
          borderColor: 'rgba(94, 96, 110, .5)',
          strokeDashArray: 4
        },
        noData: {
            text: 'Mohon Tunggu...'
        }
    }

    var chart2 = new ApexCharts(
        document.querySelector("#apexx"),
        options2
    );

    chart2.render();
}
// end grafik ruang A

// Hapus History ruang A
function hapus_a(el){
    var x = $(el).data("kode");
    var url = "https://alfianapps.000webhostapp.com/api/kirimdata.php";
    var t = "Anda yakin ingin menghapus data tanggal " + x + "?";
    swal({
      title: "PERINGATAN",
      text: t,
      icon: "warning",
      buttons: [
        'TIDAK',
        'YA'
      ],
      dangerMode: true,
    }).then(function(isConfirm) {
      if (isConfirm) {
        //ajax delete
        $.ajax({
          url:  url,
          data:   { delete_a:x },
          dataType: 'JSON',
          type: 'GET',
          success: function (response) {
            swal({
              title: 'SUKSES!',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              timer: 1000,
              showConfirmButton: false,
              showCancelButton: false,
              buttons: false,
            }).then(function() {
              location.reload();
            });
            
          }
        });

      } else {
        return true;
      }
    })
}
// END RUANG A

// ---------------------------------- FUNGSI RUANG B --------------------------------

rpm_ruang_b();
history_ruang_b();

function rpm_ruang_b(){
    $.getJSON('https://alfianapps.000webhostapp.com/api/json.php', {fungsi: "rpm_ruang_b"}, function(result){
        var g1;
        var dataa = [];
        var ml = "";
        $.each(result, function(i, k){
            var id = k.id_rpm;
            var liter_menit = k.liter_menit; //rpm
            dataa.push(liter_menit);
            $(".GaugeMeter").gaugeMeter();
        });$("#GaugeMeterRruang_b").gaugeMeter({percent:dataa});
    });
}setInterval(rpm_ruang_b, 500);        

function ml_ruang_b() {
    $.getJSON('https://alfianapps.000webhostapp.com/api/json.php', {fungsi: "rpm_ruang_b"}, function(result){
        var dataa = [];
        var ml = "";
        $.each(result, function(i, k){
            var id = k.id_rpm;
            var mili_liter = k.mili_liter;// total

            ml += mili_liter + ' mL';                    
        });
        $("#ml_ruang_b").html(ml);
    });
}setInterval(ml_ruang_b, 500);

function history_ruang_b() {
    var hasil_b = "";
    $.getJSON('https://alfianapps.000webhostapp.com/api/json.php', {fungsi: "data_monitoring_ruang_b"}, function(result){
		$.each(result, function(i, kolom){
            var id = kolom.id;
            var nilai_sensor = kolom.nilai_sensor;
            var tgl = kolom.tgl;
            var status = kolom.status;
            
            var today = new Date();
            var dd = today.getDate();

            var mm = today.getMonth()+1; 
            var yyyy = today.getFullYear();
            if(dd<10) 
            {
                dd='0'+dd;
            } 

            if(mm<10) 
            {
                mm='0'+mm;
            } 
            today = yyyy+'-'+mm+'-'+dd;

            if(tgl === today) 
            {
                hasil_b += '<a href="#" class="list-group-item" >\
                        <div class="row" style="padding: 20px;">\
                            <div class="col-20">\
                                <figure class="avatar avatar-40 bgra">\
                                    <b><span class="material-icons" style="color:#fff;">done</span></b>\
                                </figure>\
                            </div>\
                            <div class="col-80 pl-0 align-self-center">\
                                <h6 class="mb-1 font-weight-normal">\
                                    <b>'+ nilai_sensor +' Liter Air</b>\
                                </h6>\
                                <p class="small text-mute">\
                                    Pada Hari ini ('+tgl+') <br> \
                                    Status :<b> '+ status +' </b> \
                                    <span class="float-right ml-2 text-mute small popup-close" data-kode="'+tgl+'" onclick="hapus_a(this)"><i class="icon material-icons md-only">delete</i></span>\
                                </p>\
                            </div>\
                        </div>\
                    </a>';
            }else{
                hasil_b += '<a href="#" class="list-group-item" >\
                        <div class="row" style="padding: 20px;">\
                            <div class="col-20">\
                                <figure class="avatar avatar-40 bgra">\
                                    <b><span class="material-icons" style="color:#fff;">done</span></b>\
                                </figure>\
                            </div>\
                            <div class="col-80 pl-0 align-self-center">\
                                <h6 class="mb-1 font-weight-normal">\
                                    <b>'+ nilai_sensor +' Liter Air</b>\
                                </h6>\
                                <p class="small text-mute">\
                                    Pada tanggal ' + tgl + ' <br> \
                                    Status :<b> '+ status +' </b> \
                                    <span class="float-right ml-2 text-mute small popup-close" data-kode="'+tgl+'" onclick="hapus_a(this)"><i class="icon material-icons md-only">delete</i></span>\
                                </p>\
                            </div>\
                        </div>\
                    </a>';
            }
        });
		$("#data2").html(hasil_b);
	});        
}setInterval(history_ruang_b, 2500);
         
// menampilkan grafik ruang B
$.getJSON('https://alfianapps.000webhostapp.com/api/json.php', {fungsi: "data_monitoring_ruang_b"}, function(result){
    var nilai_sensorr = [];
    var tgll = [];
    $.each(result, function(i, kolom){
        var id = kolom.id;
        var nilai_sensor = kolom.nilai_sensor;
        var tgl = kolom.tgl;

        nilai_sensorr.push(nilai_sensor);
        tgll.push(tgl);
    });
    
    grafik_b(nilai_sensorr, tgll);
});

function grafik_b(nilai_sensorr, tgll) {

   var options2 = {
        chart: {
            height: 350,
            type: 'area',
        },
        dataLabels: {
            enabled: false,
        },
        stroke: {
            curve: 'smooth'
        },
        series: [
                  {
                      name: 'Penggunaan Air perLiter',
                      data: nilai_sensorr
                  } 
                ],

        xaxis: {
            type: 'datetime',
            categories: tgll,
            labels: {
              style: {
                colors: 'rgba(94, 96, 110, .5)'
              }
            }           
        },
        tooltip: {
            theme:'dark',
            x: {
                format: 'dd-MM-yyyy'
            },
        },
        grid: {
          borderColor: 'rgba(94, 96, 110, .5)',
          strokeDashArray: 4
        },
        noData: {
            text: 'Mohon Tunggu...'
        }
    }

    var chart2 = new ApexCharts(
        document.querySelector("#apexx2"),
        options2
    );

    chart2.render();
}
// end grafik ruang B

// Hapus History ruang B
function hapus_b(el){
    var x = $(el).data("kode");
    var url = "https://alfianapps.000webhostapp.com/api/kirimdata.php";
    var t = "Anda yakin ingin menghapus data tanggal " + x + "?";
    swal({
      title: "PERINGATAN",
      text: t,
      icon: "warning",
      buttons: [
        'TIDAK',
        'YA'
      ],
      dangerMode: true,
    }).then(function(isConfirm) {
      if (isConfirm) {
        //ajax delete
        $.ajax({
          url:  url,
          data:   { delete_b:x },
          dataType: 'JSON',
          type: 'GET',
          success: function (response) {
            swal({
              title: 'SUKSES!',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              timer: 1000,
              showConfirmButton: false,
              showCancelButton: false,
              buttons: false,
            }).then(function() {
              location.reload();
            });
            
          }
        });

      } else {
        return true;
      }
    })
}